package uk.ac.cam.psychometrics.papi.client.spring.exception;

public class NotAuthenticatedException extends PapiClientException {

    public NotAuthenticatedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
