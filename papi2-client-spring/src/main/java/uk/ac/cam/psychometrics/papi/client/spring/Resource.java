package uk.ac.cam.psychometrics.papi.client.spring;

public interface Resource {

    boolean isConfigured();

}
