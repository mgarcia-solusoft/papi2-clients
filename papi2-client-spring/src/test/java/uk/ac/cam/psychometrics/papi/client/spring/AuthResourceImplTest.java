package uk.ac.cam.psychometrics.papi.client.spring;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import uk.ac.cam.psychometrics.papi.client.spring.exception.NotAuthenticatedException;
import uk.ac.cam.psychometrics.papi.client.spring.exception.PapiClientException;
import uk.ac.cam.psychometrics.papi.client.spring.model.PapiToken;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthResourceImplTest {

    private static final String RESOURCE_URL = "http://localhost/auth";
    private static final String REQUEST_URL = "http://localhost/auth";
    private static final int CUSTOMER_ID = 1;
    private static final String API_KEY = "apiKey";
    private static final String TOKEN_STRING = "token";

    @Mock
    private RestTemplate restTemplate;

    private AuthResourceImpl authResource;

    @Before
    public void setUp() {
        authResource = new AuthResourceImpl(restTemplate, RESOURCE_URL);
    }

    @Test
    public void shouldReturnPapiToken() {
        PapiToken token = createPapiToken();
        when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(createResponseEntity(token));
        PapiToken returnedToken = authResource.requestToken(CUSTOMER_ID, API_KEY);
        assertEquals(token, returnedToken);
        verifyRequest();
    }

    private PapiToken createPapiToken() {
        return new PapiToken(TOKEN_STRING, System.currentTimeMillis() + 3600000);
    }

    private ResponseEntity<PapiToken> createResponseEntity(PapiToken papiToken) {
        return new ResponseEntity<>(papiToken, HttpStatus.OK);
    }

    private void verifyRequest() {
        ArgumentCaptor<String> requestUrlCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        verify(restTemplate, times(1)).postForEntity(requestUrlCaptor.capture(), httpEntityCaptor.capture(), eq(PapiToken.class));
        assertEquals(REQUEST_URL, requestUrlCaptor.getValue());
        assertEquals(CUSTOMER_ID, ((AuthResourceImpl.AuthForm) httpEntityCaptor.getValue().getBody()).getCustomerId());
        assertEquals(API_KEY, ((AuthResourceImpl.AuthForm) httpEntityCaptor.getValue().getBody()).getApiKey());
        assertEquals(MediaType.APPLICATION_JSON, httpEntityCaptor.getValue().getHeaders().getContentType());
    }

    @Test
    public void shouldThrowExceptionOnUnauthorized() {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
        try {
            authResource.requestToken(CUSTOMER_ID, API_KEY);
            fail("Was expecting exception");
        } catch (NotAuthenticatedException expected) {
        }
    }

    @Test
    public void shouldThrowExceptionOnUnknown() {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
        try {
            authResource.requestToken(CUSTOMER_ID, API_KEY);
            fail("Was expecting exception");
        } catch (PapiClientException expected) {
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldThrowExceptionOnUnknownException() {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenThrow(Exception.class);
        try {
            authResource.requestToken(CUSTOMER_ID, API_KEY);
            fail("Was expecting exception");
        } catch (PapiClientException expected) {
        }
    }

}