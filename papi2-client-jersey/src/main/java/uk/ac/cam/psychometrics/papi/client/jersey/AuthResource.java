package uk.ac.cam.psychometrics.papi.client.jersey;

import uk.ac.cam.psychometrics.papi.client.jersey.model.PapiToken;

public interface AuthResource {

    PapiToken requestToken(int customerId, String apiKey);

}
