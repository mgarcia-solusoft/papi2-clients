package uk.ac.cam.psychometrics.papi.client.jersey.model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class Prediction implements Serializable {

    private final Trait trait;
    private final double value;

    @JsonCreator
    public Prediction(@JsonProperty("trait") final Trait trait,
                      @JsonProperty("value") final double value) {
        this.trait = trait;
        this.value = value;
    }

    public Trait getTrait() {
        return trait;
    }

    public double getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(trait, value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Prediction other = (Prediction) obj;
        return Objects.equals(this.trait, other.trait) && Objects.equals(this.value, other.value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Prediction{");
        sb.append("trait='").append(trait).append('\'');
        sb.append(", value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}