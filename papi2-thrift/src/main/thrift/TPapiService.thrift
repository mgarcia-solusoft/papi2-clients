namespace java uk.ac.cam.psychometrics.papi.thrift
namespace php Papi.Thrift

include "include/exception.thrift"
include "include/model.thrift"

service TPapiService {
    model.TPapiToken requestToken(1: i32 customerId, 2: string apiKey) throws (1: exception.TNotAuthenticatedException notAuthenticatedException);
    list<model.TPredictionResult> getBatchByLikeIds(1: list<string> traits, 2: string token, 3: list<model.TUserLikeIds> userLikeIdsList) throws (1: exception.TTokenInvalidException tokenInvalidException, 2: exception.TTokenExpiredException tokenExpiredException, 3: exception.TUsageLimitExceededException usageLimitExceededException);
}